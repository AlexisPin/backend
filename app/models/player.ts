import { DateTime } from 'luxon'
import { BaseModel, belongsTo, column, hasOne, manyToMany } from '@adonisjs/lucid/orm'
import { type ManyToMany, type BelongsTo, type HasOne } from '@adonisjs/lucid/types/relations'
import Spell from '#models/spell'
import Inventory from './inventory.js'
import Dungeon from './dungeon.js'
import PersistentPlayer from './persistent_player.js'
import type { UUID } from '#types/common'

export default class Player extends BaseModel {
  @column({ isPrimary: true })
  declare id: UUID

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime | null

  @column()
  declare dungeonId: UUID | null

  @belongsTo(() => Dungeon)
  declare dungeon: BelongsTo<typeof Dungeon>

  @column()
  declare persistentPlayerId: UUID

  @belongsTo(() => PersistentPlayer)
  declare persistentPlayer: BelongsTo<typeof PersistentPlayer>

  @manyToMany(() => Spell)
  declare spells: ManyToMany<typeof Spell>

  @hasOne(() => Inventory)
  declare inventory: HasOne<typeof Inventory>

  @column()
  declare gold: number

  @column()
  declare xp: number

  @column()
  declare level: number

  @column()
  declare health: number

  @column()
  declare intelligence: number

  @column()
  declare constitution: number

  @column()
  declare wisdom: number
}
