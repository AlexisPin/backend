import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'players'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.db.rawQuery('gen_random_uuid()').knexQuery)
      table.timestamp('created_at', { useTz: false }).notNullable()
      table.timestamp('updated_at', { useTz: false }).nullable()
      table.integer('level').checkPositive().defaultTo(1)
      table.integer('xp').unsigned().defaultTo(0)
      table.integer('gold').unsigned().defaultTo(0)
      table.integer('health').unsigned().defaultTo(10)
      table.integer('intelligence').unsigned().defaultTo(10)
      table.integer('constitution').unsigned().defaultTo(10)
      table.integer('wisdom').unsigned().defaultTo(10)
      table.uuid('dungeon_id').unsigned().references('id').inTable('dungeons').onDelete('CASCADE')
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
