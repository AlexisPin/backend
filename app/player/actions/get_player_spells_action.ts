import User from '#auth/models/user'
import { inject } from '@adonisjs/core'
import GetPlayerAction from './get_player_action.js'

@inject()
export default class GetPlayerSpellsAction {
  constructor(private getPlayerAction: GetPlayerAction) { }

  async execute(user: User) {
    const player = await this.getPlayerAction.execute(user)

    await player.load('spells')

    const spells = player.spells

    return spells
  }
}
