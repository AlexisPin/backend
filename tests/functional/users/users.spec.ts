import { UserFactory } from '#database/factories/user_factory'
import db from '@adonisjs/lucid/services/db'
import { test } from '@japa/runner'

test.group('Users', (group) => {
  group.each.setup(async () => {
    await db.beginGlobalTransaction('pg')
    return () => db.rollbackGlobalTransaction('pg')
  })

  test('ensure we can get logged in user details', async ({ assert, client }) => {
    const user = await UserFactory.create()
    const response = await client.get('/auth/me').loginAs({
      web: user,
    })

    response.assertStatus(200)
    assert.equal(response.body().id, user.id)
  })

  test('ensure auth check returns correct status when guest', async ({ client }) => {
    const response = await client.get('/auth/check')

    response.assertStatus(200)
    response.assertBodyContains({ authenticated: false })
  })

  test('ensure auth check returns correct status when auth', async ({ client }) => {
    const user = await UserFactory.create()
    const response = await client.get('/auth/check').withGuard('web').loginAs(user)

    response.assertStatus(200)
    response.assertBodyContains({ authenticated: true })
  })
})

test.group('Users | Login / Logout', (group) => {
  group.each.setup(async () => {
    await db.beginGlobalTransaction('pg')
    return () => db.rollbackGlobalTransaction('pg')
  })

  test('ensure we can login user', async ({ client }) => {
    const user = await UserFactory.create()
    const response = await client
      .post('/auth/login')
      .json({ email: user.email, password: 'password' })

    response.assertStatus(204)
  })

  test('ensure throw error when login with invalid password', async ({ client }) => {
    const user = await UserFactory.create()
    const response = await client
      .post('/auth/login')
      .json({ email: user.email, password: 'invalid' })

    response.assertStatus(400)
    response.assertBodyContains({
      code: 'E_INVALID_CREDENTIALS',
      message: 'No account can be found with the provided credentials.',
    })
  })

  test('ensure throw error when login with invalid email', async ({ client }) => {
    const response = await client
      .post('/auth/login')
      .json({ email: 'test.user@example.com', password: 'invalid' })

    response.assertStatus(400)
    response.assertBodyContains({
      code: 'E_INVALID_CREDENTIALS',
      message: 'No account can be found with the provided credentials.',
    })
  })

  test('ensure that we can log out a user', async ({ client }) => {
    const user = await UserFactory.create()
    const response = await client.delete('/auth/logout').withGuard('web').loginAs(user)

    response.assertStatus(204)
  })
})

test.group('Users | Register', (group) => {
  group.each.setup(async () => {
    await db.beginGlobalTransaction('pg')
    return () => db.rollbackGlobalTransaction('pg')
  })

  test('ensure user can register', async ({ client }) => {
    const reponse = await client.post('/auth/register').json({
      email: 'test.user@example.com',
      username: 'testuser',
      password: 'password',
      password_confirmation: 'password',
    })

    reponse.assertStatus(201)
  })

  test('ensure user cannot register with an existing email', async ({ client }) => {
    await UserFactory.merge({ email: 'test.user@example.com' }).create()

    const response = await client.post('/auth/register').json({
      email: 'test.user@example.com',
      username: 'testuser',
      password: 'password',
      password_confirmation: 'password',
    })

    response.assertStatus(422)
    response.assertBodyContains({
      errors: [
        {
          field: 'email',
          rule: 'unique',
          message: 'An account already exists with the provided email.',
        },
      ],
    })
  })

  test('ensure user cannot register with an invalid email', async ({ client }) => {
    const response = await client.post('/auth/register').json({
      email: 'test.user',
      username: 'testuser',
      password: 'password',
      password_confirmation: 'password',
    })

    response.assertStatus(422)
    response.assertBodyContains({
      errors: [
        {
          field: 'email',
          rule: 'email',
          message: 'Your email is not valid.',
        },
      ],
    })
  })

  test('ensure user cannot register with a short password', async ({ client }) => {
    const response = await client.post('/auth/register').json({
      email: 'test.user@example.com',
      username: 'testuser',
      password: 'secret',
      password_confirmation: 'secret',
    })

    response.assertStatus(422)

    response.assertBodyContains({
      errors: [
        {
          field: 'password',
          rule: 'minLength',
          message: 'Your password needs to be at least 8 characters long.',
        },
      ],
    })
  })

  test('ensure user cannot register with a password that does not match', async ({ client }) => {
    const response = await client.post('/auth/register').json({
      email: 'test.user@example.com',
      username: 'testuser',
      password: 'password',
      password_confirmation: 'password5',
    })

    response.assertStatus(422)
    response.assertBodyContains({
      errors: [
        {
          field: 'password',
          rule: 'confirmed',
          message: 'Passwords do not match.',
        },
      ],
    })
  })

  test('ensure user cannot register with a username that is already taken', async ({ client }) => {
    await UserFactory.merge({ username: 'testuser' }).create()

    const response = await client.post('/auth/register').json({
      email: 'test.user@example.com',
      username: 'testuser',
      password: 'password',
      password_confirmation: 'password',
    })

    response.assertStatus(422)
    response.assertBodyContains({
      errors: [
        {
          field: 'username',
          rule: 'unique',
          message: 'An account already exists with the provided username.',
        },
      ],
    })
  })

  test('ensure user cannot register with a username that is too short', async ({ client }) => {
    const response = await client.post('/auth/register').json({
      email: 'test.user@example.com',
      username: 'use',
      password: 'password',
      password_confirmation: 'password',
    })

    response.assertStatus(422)
    response.assertBodyContains({
      errors: [
        {
          field: 'username',
          rule: 'minLength',
          message: 'Your username needs to be at least 4 characters long.',
        },
      ],
    })
  })

  test('ensure user cannot register with a username that is too long', async ({ client }) => {
    const response = await client.post('/auth/register').json({
      email: 'test.user@example.com',
      username: 'testusertestusertestusertestusertestusertestusertestusertestuser',
      password: 'password',
      password_confirmation: 'password',
    })

    response.assertStatus(422)
    response.assertBodyContains({
      errors: [
        {
          field: 'username',
          rule: 'maxLength',
          message: 'Your username cannot be longer than 25 characters.',
        },
      ],
    })
  })
})
