import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'dungeon_items'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.db.rawQuery('gen_random_uuid()').knexQuery)
      table.timestamp('created_at', { useTz: false })
      table.timestamp('updated_at', { useTz: false })
      table
        .uuid('dungeon_stage_id')
        .unsigned()
        .references('id')
        .inTable('dungeon_stages')
        .onDelete('CASCADE')
      table.uuid('item_id').unsigned().references('id').inTable('items').onDelete('CASCADE')
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
