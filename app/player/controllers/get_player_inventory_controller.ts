import { HttpContext } from '@adonisjs/core/http'
import { assert } from '../../utils.js'
import { inject } from '@adonisjs/core'
import GetPlayerInventoryAction from '#player/actions/get_player_inventory_action'
import PlayerInventoryPresenter from '#player/presenters/player_inventory_presenter'

@inject()
export default class GetPlayerInventoryController {
  constructor(
    private action: GetPlayerInventoryAction,
    private presenter: PlayerInventoryPresenter
  ) { }

  async handle({ auth, response }: HttpContext) {
    const user = auth.user
    assert(user, 'User must be logged in to get inventory player')

    const inventory = await this.action.execute(user)

    return response.ok(await this.presenter.json(inventory))
  }
}
