import User from '#auth/models/user'
import PersistentPlayer from '#models/persistent_player'

export default class GetPersistentPlayerAction {
  async execute(user: User) {
    let player = await PersistentPlayer.findBy('userId', user.id)

    if (!player) {
      player = await user.related('persistentPlayer').create({
        gold: 0,
      })
    }

    return player
  }
}
