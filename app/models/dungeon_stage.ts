import { DateTime } from 'luxon'
import { BaseModel, belongsTo, column, hasOne } from '@adonisjs/lucid/orm'
import { type HasOne, type BelongsTo } from '@adonisjs/lucid/types/relations'
import DungeonMonster from './dungeon_monster.js'
import DungeonItem from './dungeon_item.js'
import Dungeon from './dungeon.js'
import type { UUID } from '#types/common'

export default class DungeonStage extends BaseModel {
  @column({ isPrimary: true })
  declare id: UUID

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime

  @column()
  declare dungeonId: UUID

  @belongsTo(() => Dungeon)
  declare dungeon: BelongsTo<typeof Dungeon>

  @column()
  declare level: number

  @column()
  declare name: string

  @hasOne(() => DungeonMonster)
  declare monster: HasOne<typeof DungeonMonster>

  @hasOne(() => DungeonItem)
  declare item: HasOne<typeof DungeonItem>
}
