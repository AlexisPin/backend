import app from '@adonisjs/core/services/app'
import { HttpContext, ExceptionHandler } from '@adonisjs/core/http'
import { errors } from '@vinejs/vine'
import { DateTime } from 'luxon'

export default class HttpExceptionHandler extends ExceptionHandler {
  /**
   * In debug mode, the exception handler will display verbose errors
   * with pretty printed stack traces.
   */
  protected debug = !app.inProduction

  /**
   * Status pages are used to display a custom HTML pages for certain error
   * codes. You might want to enable them in production only, but feel
   * free to enable them in development as well.
   */
  protected renderStatusPages = app.inProduction

  /**
   * The method is used for handling errors and returning
   * response to the client
   */
  async handle(error: unknown, ctx: HttpContext) {
    if (error instanceof errors.E_VALIDATION_ERROR) {
      return ctx.response.status(error.status).send({ errors: error.messages })
    }
    if (
      ['E_INVALID_AUTH_UID', 'E_INVALID_AUTH_PASSWORD', 'E_INVALID_CREDENTIALS'].includes(
        error.code
      )
    ) {
      return ctx.response.badRequest({
        code: 'E_INVALID_CREDENTIALS',
        message: 'No account can be found with the provided credentials.',
      })
    }
    if (
      (error.code === 'E_AUTHORIZATION_FAILURE' && error.status === 404) ||
      error.code === 'E_ROW_NOT_FOUND'
    ) {
      return ctx.response.notFound({
        status: 404,
        path: ctx.request.url(),
        timestamp: DateTime.local(),
        code: 'E_RESSOURCE_NOT_FOUND',
        message: 'The requested ressource was not found',
        detail: 'Ensure that the ressource exists and that you have the correct permissions',
      })
    }
    if (error.code === 'E_ROUTE_NOT_FOUND') {
      return ctx.response.notFound({
        status: 404,
        path: ctx.request.url(),
        timestamp: DateTime.local(),
        code: 'E_ROUTE_NOT_FOUND',
        message: 'The requested route was not found',
        detail: 'Ensure that the route exists',
      })
    }

    return super.handle(error, ctx)
  }

  /**
   * The method is used to report error to the logging service or
   * the a third party error monitoring service.
   *
   * @note You should not attempt to send a response from this method.
   */
  async report(error: unknown, ctx: HttpContext) {
    if (this.shouldReport(error) && app.inDev) {
      console.error(error)
      return
    }
    if (!this.shouldReport(error) && !app.inProduction) {
      return
    }

    return super.report(error, ctx)
  }
}
