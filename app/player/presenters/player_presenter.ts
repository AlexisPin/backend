import Player from '#models/player'

export default class PlayerPresenter {
  json(player: Player) {
    return {
      data: {
        id: player.id,
        level: player.level,
        gold: player.gold,
        xp: player.xp,
        health: player.health,
        wisdom: player.wisdom,
        intelligence: player.intelligence,
        constitution: player.constitution,
        dungeonId: player.dungeonId,
      },
    }
  }
}
