import { BaseSeeder } from '@adonisjs/lucid/seeders'
import { S3Driver } from '../../app/drivers/s3.js'
import env from '#start/env'
import logger from '@adonisjs/core/services/logger'
import { createReadStream } from 'node:fs'
import { items } from '../../data/items.js'
import Item from '#models/item'

const config = {
  key: env.get('AWS_KEY'),
  secret: env.get('AWS_SECRET'),
  token: env.get('AWS_TOKEN'),
  bucket: env.get('AWS_BUCKET'),
  endpoint: env.get('AWS_ENDPOINT'),
  region: env.get('AWS_REGION'),
  driver: 's3' as const,
  visibility: 'private' as const,
}

export default class extends BaseSeeder {
  async run() {
    //const spellFilenames = await fsReadAll(new URL('../../images/items', import.meta.url))
    const driver = new S3Driver(config, logger)
    for (const item of items) {
      const { description, name, price } = item
      const filename = 'Fire2.webp'
      const file = createReadStream(new URL(`../../images/items/${filename}`, import.meta.url))
      await driver.putStream(`items/${filename}`, file)
      await Item.create({
        cover: JSON.stringify(`items/${filename}`),
        name,
        description,
        price,
      })
    }
  }
}
