import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'player_spell'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.db.rawQuery('gen_random_uuid()').knexQuery)
      table.uuid('spell_id').unsigned().references('id').inTable('spells').onDelete('CASCADE')
      table.uuid('player_id').unsigned().references('id').inTable('players').onDelete('CASCADE')
      table.integer('index').checkPositive().checkBetween([1, 4]).notNullable()
      table.unique(['spell_id', 'player_id'])
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
