import PersistentPlayer from '#models/persistent_player'

export default class PersistentPlayerPresenter {
  json(player: PersistentPlayer) {
    return {
      data: {
        id: player.id,
        gold: player.gold,
      },
    }
  }
}
