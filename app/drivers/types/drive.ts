export type ContentHeaders = {
  contentType?: string
  contentLanguage?: string
  contentEncoding?: string
  contentDisposition?: string
  cacheControl?: string
  contentLength?: string | number
}

/**
 * Options for writing, moving and copying
 * files
 */
export type WriteOptions = {
  visibility?: string
} & ContentHeaders &
  Record<string, any>

/**
 * Stats returned by the drive drivers
 */
export type DriveFileStats = {
  size: number
  modified: Date
  isFile: boolean
  etag?: string
}

/**
 * Available visibilities
 */
export type Visibility = 'public' | 'private'
