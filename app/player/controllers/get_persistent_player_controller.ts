import { inject } from '@adonisjs/core'
import { HttpContext } from '@adonisjs/core/http'
import GetPersistentPlayerAction from '#player/actions/get_persistent_player_action'
import { assert } from '../../utils.js'
import PersistentPlayerPresenter from '#player/presenters/persistent_player_presenter'

@inject()
export default class GetPersistentPlayerController {
  constructor(
    private action: GetPersistentPlayerAction,
    private presenter: PersistentPlayerPresenter
  ) { }

  async handle({ auth, response }: HttpContext) {
    const user = auth.user
    assert(user, 'User must be logged in to get persistant player')
    
    const player = await this.action.execute(user)

    return response.ok(this.presenter.json(player))
  }
}
