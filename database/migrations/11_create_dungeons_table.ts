import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'dungeons'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.db.rawQuery('gen_random_uuid()').knexQuery)
      table.timestamp('created_at', { useTz: false })
      table.timestamp('updated_at', { useTz: false })
      table
        .integer('type_id')
        .unsigned()
        .references('id')
        .inTable('dungeon_types')
        .onDelete('CASCADE')
      table
        .integer('rank_id')
        .unsigned()
        .references('id')
        .inTable('dungeon_ranks')
        .onDelete('CASCADE')
      table.integer('stage_number').unsigned()
      table.string('name').notNullable()
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
