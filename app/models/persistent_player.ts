import { DateTime } from 'luxon'
import { BaseModel, belongsTo, column, hasOne } from '@adonisjs/lucid/orm'
import Player from './player.js'
import { type BelongsTo, type HasOne } from '@adonisjs/lucid/types/relations'
import User from '#auth/models/user'
import type { UUID } from '#types/common'

export default class PersistentPlayer extends BaseModel {
  @column({ isPrimary: true })
  declare id: UUID

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime

  @column()
  declare gold: number

  @hasOne(() => Player)
  declare player: HasOne<typeof Player>

  @column()
  declare userId: UUID

  @belongsTo(() => User)
  declare user: BelongsTo<typeof User>
}
