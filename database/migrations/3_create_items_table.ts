import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'items'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.db.rawQuery('gen_random_uuid()').knexQuery)
      table.string('name').notNullable()
      table.string('description').notNullable()
      table.json('cover').notNullable()
      table.integer('price').checkPositive().notNullable()
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
