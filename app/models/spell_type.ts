import { BaseModel, column, hasMany } from '@adonisjs/lucid/orm'
import Spell from './spell.js'
import type { HasMany } from '@adonisjs/lucid/types/relations'

export default class SpellType extends BaseModel {
  @column({ isPrimary: true })
  declare id: number

  @column()
  declare name: string

  @column()
  declare description: string

  @column()
  declare cover: string

  @hasMany(() => Spell)
  declare spells: HasMany<typeof Spell>
}
