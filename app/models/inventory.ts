import { BaseModel, belongsTo, column, manyToMany } from '@adonisjs/lucid/orm'
import type { BelongsTo, ManyToMany } from '@adonisjs/lucid/types/relations'
import Player from './player.js'
import Item from './item.js'
import type { UUID } from '#types/common'

export default class Inventory extends BaseModel {
  @column({ isPrimary: true })
  declare id: UUID

  @manyToMany(() => Item, {
    pivotColumns: ['quantity'],
  })
  declare items: ManyToMany<typeof Item>

  @column()
  declare playerId: UUID

  @belongsTo(() => Player)
  declare player: BelongsTo<typeof Player>
}
