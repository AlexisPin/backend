import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'spell_model'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.db.rawQuery('gen_random_uuid()').knexQuery)
      table.timestamp('created_at', { useTz: false })
      table.timestamp('updated_at', { useTz: false })
      table.uuid('model_id').references('id').inTable('models').onDelete('CASCADE')
      table.uuid('spell_id').references('id').inTable('spells').onDelete('CASCADE')
      table.unique(['model_id', 'spell_id'])
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}