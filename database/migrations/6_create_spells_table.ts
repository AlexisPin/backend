import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'spells'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.db.rawQuery('gen_random_uuid()').knexQuery)
      table.string('name').notNullable()
      table.string('description').notNullable()
      table.string('cover').notNullable()
      table
        .integer('spell_type_id')
        .unsigned()
        .references('id')
        .inTable('spell_types')
        .onDelete('CASCADE')
      table.integer('damage').unsigned().notNullable()
      table.integer('healing').unsigned().notNullable()
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
