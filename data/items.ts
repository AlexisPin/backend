export const items = [
  {
    name: 'Baguette magique',
    description:
      'Une baguette magique est un outil essentiel pour tout magicien. Elle permet de lancer des sorts et de contrôler les forces de la magie.',
    price: 100,
  },
  {
    name: 'Grimoire',
    description:
      'Un grimoire est un livre de magie qui contient des sorts, des formules et des connaissances magiques. Il est indispensable pour tout magicien qui souhaite apprendre la magie.',
    price: 50,
  },
  {
    name: 'Pendentif magique',
    description:
      'Un pendentif magique est un bijou qui possède des pouvoirs magiques. Il peut être utilisé pour lancer des sorts, se protéger des dangers ou simplement pour se faire remarquer.',
    price: 25,
  },
  {
    name: 'Pierre magique',
    description:
      'Une pierre magique est une pierre précieuse qui possède des pouvoirs magiques. Elle peut être utilisée pour lancer des sorts, se protéger des dangers ou simplement pour ajouter une touche de magie à son apparence.',
    price: 1000,
  },
  {
    name: 'Potion magique',
    description:
      'Une potion magique est un liquide qui possède des pouvoirs magiques. Elle peut être utilisée pour guérir, soigner, ou même pour donner des pouvoirs surnaturels.',
    price: 50,
  },
  {
    name: 'Livre de sorts',
    description:
      'Un livre de sorts est un livre qui contient des sorts, des formules et des connaissances magiques. Il est indispensable pour tout magicien qui souhaite apprendre de nouveaux sorts.',
    price: 250,
  },
  {
    name: 'Coffret de magie',
    description:
      "Un coffret de magie est un ensemble d'objets magiques qui peuvent être utilisés pour lancer des sorts, se protéger des dangers ou simplement pour ajouter une touche de magie à son apparence.",
    price: 10000,
  },
  {
    name: 'Amulette de puissance',
    description:
      'Cette amulette en or ornée de pierres précieuses augmente les dégâts de tous les sorts du porteur de 1d6.',
    price: 1000,
  },
  {
    name: 'Cercle de guérison',
    description:
      'Ce cercle magique en bois entoure le porteur et lui permet de soigner 1d8 points de vie supplémentaires par tour.',
    price: 500,
  },
  {
    name: 'Pierre de force',
    description:
      'Cette pierre précieuse rouge augmente la puissance des sorts de feu du porteur de 1d4.',
    price: 300,
  },
  {
    name: 'Elixir de vie',
    description:
      'Cet élixir vert guérit le porteur de 10 points de vie et lui donne un bonus de +1 à ses jets de guérison pendant une heure.',
    price: 200,
  },
  {
    name: 'Fiole de poison',
    description:
      'Cette fiole de poison bleu augmente les dégâts des sorts de poison du porteur de 1d2.',
    price: 100,
  },
]
