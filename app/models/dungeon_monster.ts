import { DateTime } from 'luxon'
import { BaseModel, belongsTo, column } from '@adonisjs/lucid/orm'
import DungeonStage from './dungeon_stage.js'
import { type BelongsTo } from '@adonisjs/lucid/types/relations'
import Monster from './monster.js'
import type { UUID } from '#types/common'

export default class DungeonMonster extends BaseModel {
  @column({ isPrimary: true })
  declare id: UUID

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime

  @column()
  declare level: number

  @column()
  declare monsterId: UUID

  @belongsTo(() => Monster)
  declare monster: BelongsTo<typeof Monster>

  @column()
  declare remainingHp: number

  @column()
  declare spellAttackEffect: number

  @column()
  declare spellDefenseEffect: number

  @column()
  declare dungeonStageId: UUID

  @belongsTo(() => DungeonStage)
  declare dungeonStage: BelongsTo<typeof DungeonStage>
}
