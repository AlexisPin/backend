import { Opaque } from '@poppinss/utils/types'

export type UUID = Opaque<string, 'UUID'>
