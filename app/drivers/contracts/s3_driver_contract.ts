import { S3Client, S3ClientConfig } from '@aws-sdk/client-s3'
import { ContentHeaders, DriveFileStats, Visibility, WriteOptions } from '../types/drive.js'

export type S3DriverConfig = S3ClientConfig & {
  driver: 's3'
  visibility?: Visibility
  bucket: string
  cdnUrl?: string
  key?: string
  secret?: string
  token?: string
}

export interface S3DriverContract extends DriverContract {
  name: 's3'
  adapter: S3Client

  /**
   * Returns a new instance of the s3 driver with a custom runtime
   * bucket
   */
  bucket(bucket: string): S3DriverContract
}

export interface DriverContract {
  /**
   * Name of the driver
   */
  name: string

  /**
   * A boolean to find if the location path exists or not
   */
  exists(location: string): Promise<boolean>

  /**
   * Returns the file contents as a buffer.
   */
  get(location: string): Promise<Buffer>

  /**
   * Returns the file contents as a stream
   */
  getStream(location: string): Promise<NodeJS.ReadableStream>

  /**
   * Returns the location path visibility
   */
  getVisibility(location: string): Promise<Visibility>

  /**
   *  Update the visibility of the file
   */
  setVisibility(location: string, visibility: Visibility): Promise<void>

  /**
   * Returns the location path stats
   */
  getStats(location: string): Promise<DriveFileStats>

  /**
   * Returns a signed URL for a given location path
   */
  getSignedUrl(
    location: string,
    options?: ContentHeaders & { expiresIn?: string | number }
  ): Promise<string>

  /**
   * Returns a URL for a given location path
   */
  getUrl(location: string): Promise<string>

  /**
   * Write string|buffer contents to a destination. The missing
   * intermediate directories will be created (if required).
   */
  put(location: string, contents: Buffer | string, options?: WriteOptions): Promise<void>

  /**
   * Write a stream to a destination. The missing intermediate
   * directories will be created (if required).
   */
  putStream(
    location: string,
    contents: NodeJS.ReadableStream,
    options?: WriteOptions
  ): Promise<void>

  /**
   * Remove a given location path
   */
  delete(location: string): Promise<void>

  /**
   * Copy a given location path from the source to the desination.
   * The missing intermediate directories will be created (if required)
   */
  copy(source: string, destination: string, options?: WriteOptions): Promise<void>

  /**
   * Move a given location path from the source to the desination.
   * The missing intermediate directories will be created (if required)
   */
  move(source: string, destination: string, options?: WriteOptions): Promise<void>
}
