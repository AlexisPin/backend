import { BaseSeeder } from '@adonisjs/lucid/seeders'
import { S3Driver } from '../../app/drivers/s3.js'
import env from '#start/env'
import logger from '@adonisjs/core/services/logger'
import { createReadStream } from 'node:fs'
import Spell from '#models/spell'
import { spells } from '../../data/spells.js'

const config = {
  key: env.get('AWS_KEY'),
  secret: env.get('AWS_SECRET'),
  token: env.get('AWS_TOKEN'),
  bucket: env.get('AWS_BUCKET'),
  endpoint: env.get('AWS_ENDPOINT'),
  region: env.get('AWS_REGION'),
  driver: 's3' as const,
  visibility: 'private' as const,
}

export default class extends BaseSeeder {
  async run() {
    //const spellFilenames = await fsReadAll(new URL('../../images/spells', import.meta.url))
    const driver = new S3Driver(config, logger)
    for (const spell of spells) {
      const { name, damage, description, healing, spellTypeId } = spell
      const filename = 'Ice-_2_.webp'
      const file = createReadStream(new URL(`../../images/spells/${filename}`, import.meta.url))
      await driver.putStream(`spells/${filename}`, file)
      await Spell.create({
        cover: `spells/${filename}`,
        name,
        description,
        damage,
        healing,
        spellTypeId,
      })
    }
  }
}
