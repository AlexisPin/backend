import env from '#start/env'
import { test } from '@japa/runner'
import string from '@poppinss/utils/string'
import { S3Driver } from '../../../app/drivers/s3.js'
import logger from '@adonisjs/core/services/logger'
import { HeadObjectCommand } from '@aws-sdk/client-s3'
import { Filesystem } from '@poppinss/dev-utils'
import { dirname, join } from 'node:path'
import { fileURLToPath } from 'node:url'
import { Readable } from 'node:stream'

const fs = new Filesystem(join(dirname(fileURLToPath(import.meta.url)), '__app'))

const config = {
  key: env.get('AWS_KEY'),
  secret: env.get('AWS_SECRET'),
  token: env.get('AWS_TOKEN'),
  bucket: env.get('AWS_BUCKET'),
  endpoint: env.get('AWS_ENDPOINT'),
  region: env.get('AWS_REGION'),
  driver: 's3' as const,
  visibility: 'private' as const,
}

test.group('S3 driver | put', () => {
  test('write file to the destination', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`
    const driver = new S3Driver(config, logger)

    await driver.put(fileName, 'Hello world')

    await driver.getUrl(fileName)

    const contents = await driver.get(fileName)
    assert.equal(contents.toString(), 'Hello world')

    await driver.delete(fileName)
  }).timeout(6000)

  test('write to nested path', async ({ assert }) => {
    const fileName = `bar/baz/${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'Hello World')

    const contents = await driver.get(fileName)
    assert.equal(contents.toString(), 'Hello World')

    await driver.delete(fileName)
  }).timeout(6000)

  test('overwrite destination when already exists', async ({ assert }) => {
    const fileName = `bar/baz/${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'hello world')
    await driver.put(fileName, 'hi world')

    const contents = await driver.get(fileName)
    assert.equal(contents.toString(), 'hi world')

    await driver.delete(fileName)
  }).timeout(6000)

  test('set custom content-type for the file', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)

    await driver.put(fileName, '{ "hello": "world" }', {
      contentType: 'application/json',
    })

    const response = await driver.adapter.send(
      new HeadObjectCommand({ Key: fileName, Bucket: config.bucket })
    )

    assert.equal(response.ContentType, 'application/json')
    await driver.delete(fileName)
  }).timeout(6000)

  test('switch bucket at runtime', async ({ assert }) => {
    const AWS_BUCKET = config.bucket

    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(
      {
        ...config,
        bucket: 'foo',
      },
      logger
    )

    await driver.bucket(AWS_BUCKET).put(fileName, 'hello world')
    await driver.bucket(AWS_BUCKET).getUrl(fileName)

    const contents = await driver.bucket(AWS_BUCKET).get(fileName)
    assert.equal(contents.toString(), 'hello world')

    await driver.bucket(AWS_BUCKET).delete(fileName)
  }).timeout(6000)
})

test.group('S3 driver | putStream', (group) => {
  group.each.teardown(async () => {
    await fs.cleanup()
  })

  test('write file to the destination', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await fs.add('foo.txt', 'hello stream')
    const stream = fs.fsExtra.createReadStream(join(fs.basePath, 'foo.txt'))

    await driver.putStream(fileName, stream)

    const contents = await driver.get(fileName)
    assert.equal(contents.toString(), 'hello stream')

    await driver.delete(fileName)
  }).timeout(6000)

  test('write to nested path', async ({ assert }) => {
    const fileName = `bar/baz/${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await fs.add('foo.txt', 'hello stream')
    const stream = fs.fsExtra.createReadStream(join(fs.basePath, 'foo.txt'))
    await driver.putStream(fileName, stream)

    const contents = await driver.get(fileName)
    assert.equal(contents.toString(), 'hello stream')

    await driver.delete(fileName)
  }).timeout(6000)

  test('overwrite destination when already exists', async ({ assert }) => {
    const fileName = `bar/baz/${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await fs.add('foo.txt', 'hi stream')
    const stream = fs.fsExtra.createReadStream(join(fs.basePath, 'foo.txt'))
    await driver.put(fileName, 'hello world')
    await driver.putStream(fileName, stream)

    const contents = await driver.get(fileName)
    assert.equal(contents.toString(), 'hi stream')

    await driver.delete(fileName)
  }).timeout(6000)

  test('set custom content-type for the file', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await fs.add('foo.txt', '{ "hello": "world" }')
    const stream = fs.fsExtra.createReadStream(join(fs.basePath, 'foo.txt'))
    await driver.putStream(fileName, stream, {
      contentType: 'application/json',
    })

    const response = await driver.adapter.send(
      new HeadObjectCommand({ Key: fileName, Bucket: config.bucket })
    )
    assert.equal(response.ContentType, 'application/json')

    await driver.delete(fileName)
  }).timeout(6000)
})

test.group('S3 driver | exists', () => {
  test('return true when a file exists', async ({ assert }) => {
    const fileName = `bar/baz/${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)

    await driver.put(fileName, 'bar')
    assert.isTrue(await driver.exists(fileName))

    await driver.delete(fileName)
  }).timeout(6000)

  test("return false when a file doesn't exists", async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    assert.isFalse(await driver.exists(fileName))
  }).timeout(6000)

  test("return false when a file parent directory doesn't exists", async ({ assert }) => {
    const fileName = `bar/baz/${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    assert.isFalse(await driver.exists(fileName))
  }).timeout(6000)

  test('raise exception when credentials are incorrect', async ({ assert }) => {
    assert.plan(1)

    const driver = new S3Driver(
      {
        ...config,
        key: 'foo',
        secret: 'bar',
      },
      logger
    )
    try {
      await driver.exists('bar/baz/foo.txt')
    } catch (error) {
      assert.equal(error.original.$metadata.httpStatusCode, 403)
    }
  }).timeout(6000)
})

test.group('S3 driver | delete', (group) => {
  group.each.teardown(async () => {
    await fs.cleanup()
  })

  test('remove file', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'bar')
    await driver.delete(fileName)

    assert.isFalse(await driver.exists(fileName))
  }).timeout(6000)

  test('do not error when trying to remove a non-existing file', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.delete(fileName)
    assert.isFalse(await driver.exists(fileName))
  }).timeout(6000)

  test("do not error when file parent directory doesn't exists", async ({ assert }) => {
    const fileName = `bar/baz/${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)

    await driver.delete(fileName)
    assert.isFalse(await driver.exists(fileName))
  }).timeout(6000)
})

test.group('S3 driver | copy', (group) => {
  group.each.teardown(async () => {
    await fs.cleanup()
  })

  test('copy file from within the disk root', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`
    const fileName1 = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)

    await driver.put(fileName, 'hello world')
    await driver.copy(fileName, fileName1)

    const contents = await driver.get(fileName1)
    assert.equal(contents.toString(), 'hello world')

    await driver.delete(fileName)
    await driver.delete(fileName1)
  }).timeout(6000)

  test('create intermediate directories when copying a file', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`
    const fileName1 = `baz/${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)

    await driver.put(fileName, 'hello world')
    await driver.copy(fileName, fileName1)

    const contents = await driver.get(fileName1)
    assert.equal(contents.toString(), 'hello world')

    await driver.delete(fileName)
    await driver.delete(fileName1)
  }).timeout(6000)

  test("return error when source doesn't exists", async ({ assert }) => {
    assert.plan(2)

    const driver = new S3Driver(config, logger)

    try {
      await driver.copy('foo.txt', 'bar.txt')
    } catch (error) {
      assert.equal(error.message, 'Cannot copy file from "foo.txt" to "bar.txt"')
      assert.equal(error.code, 'E_CANNOT_COPY_FILE')
    }
  }).timeout(6000)
})

test.group('S3 driver | get', (group) => {
  group.each.teardown(async () => {
    await fs.cleanup()
  })

  test('get file contents', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'hello world')

    const contents = await driver.get(fileName)
    assert.equal(contents.toString(), 'hello world')

    await driver.delete(fileName)
  }).timeout(6000)

  test('get file contents as a stream', async ({ assert }, done) => {
    assert.plan(2)

    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'hello world')

    const stream = await driver.getStream(fileName)
    assert.instanceOf(stream, Readable)

    stream.on('data', (chunk) => {
      assert.equal(chunk, 'hello world')
    })
    stream.on('end', async () => {
      await driver.delete(fileName)
      done()
    })
    stream.on('error', (error) => {
      done(error)
    })
  })
    .timeout(6000)
    .waitForDone()

  test("return error when file doesn't exists", async ({ assert }) => {
    assert.plan(2)

    const driver = new S3Driver(config, logger)

    try {
      await driver.get('foo.txt')
    } catch (error) {
      assert.equal(error.message, 'Cannot read file from location "foo.txt"')
      assert.equal(error.code, 'E_CANNOT_READ_FILE')
    }
  }).timeout(6000)
})

test.group('S3 driver | getStats', (group) => {
  group.each.teardown(async () => {
    await fs.cleanup()
  })

  test('get file stats', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'hello world')

    const stats = await driver.getStats(fileName)
    assert.equal(stats.size, 11)
    assert.instanceOf(stats.modified, Date)

    await driver.delete(fileName)
  }).timeout(6000)

  test('return error when file is missing', async ({ assert }) => {
    assert.plan(2)

    const driver = new S3Driver(config, logger)
    const fileName = `${string.random(10)}.txt`

    try {
      await driver.getStats(fileName)
    } catch (error) {
      assert.equal(
        error.message,
        `Unable to retrieve the "stats" for file at location "${fileName}"`
      )
      assert.equal(error.code, 'E_CANNOT_GET_METADATA')
    }
  }).timeout(6000)
})

test.group('S3 driver | getUrl', (group) => {
  group.each.teardown(async () => {
    await fs.cleanup()
  })

  test('get url to a given file', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'hello world')

    const url = await driver.getUrl(fileName)

    const endpoint = new URL(config.endpoint || 'https://s3.amazonaws.com').host
    assert.equal(url, `https://${config.bucket}.${endpoint}/${fileName}`)

    await driver.delete(fileName)
  }).timeout(6000)

  test('deny access to private files', async ({ assert }) => {
    assert.plan(1)

    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'hello world')

    const url = await driver.getUrl(fileName)

    await fetch(url).then((res) => assert.equal(res.status, 403))

    await driver.delete(fileName)
  }).timeout(6000)
})

test.group('S3 driver | getSignedUrl', (group) => {
  group.each.teardown(async () => {
    await fs.cleanup()
  })

  test('get signed url to a file in private disk', async ({ assert }) => {
    assert.plan(2)

    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'hello world')

    await fetch(await driver.getUrl(fileName)).then((res) => assert.equal(res.status, 403))

    const response = await fetch(await driver.getSignedUrl(fileName))

    assert.equal(await response.text(), 'hello world')

    await driver.delete(fileName)
  }).timeout(6000)

  test('define custom content headers for the file', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'hello world')

    const signedUrl = await driver.getSignedUrl(fileName, {
      contentType: 'application/json',
      contentDisposition: 'attachment',
    })

    const response = await fetch(signedUrl)

    assert.equal(response.headers.get('content-type'), 'application/json')
    assert.equal(response.headers.get('content-disposition'), 'attachment')
    assert.equal(await response.text(), 'hello world')
    await driver.delete(fileName)
  }).timeout(6000)

  test('get signed url with expiration', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)

    const signedUrl = await driver.getSignedUrl(fileName, {
      expiresIn: '2min',
    })

    const url = new URL(signedUrl)
    const expiresResult = url.searchParams.get('X-Amz-Expires')

    assert.equal(expiresResult, '120')
  }).timeout(6000)
})

test.group('S3 driver | getVisibility', (group) => {
  group.each.teardown(async () => {
    await fs.cleanup()
  })

  test('get visibility for private file', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'hello world')

    const visibility = await driver.getVisibility(fileName)
    assert.equal(visibility, 'private')

    await driver.delete(fileName)
  }).timeout(6000)

  test('get visibility for public file', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(
      {
        ...config,
        visibility: 'public',
      },
      logger
    )
    await driver.put(fileName, 'hello world')

    const visibility = await driver.getVisibility(fileName)
    assert.equal(visibility, 'public')

    await driver.delete(fileName)
  }).timeout(6000)

  test('return error when file is missing', async ({ assert }) => {
    assert.plan(2)

    const driver = new S3Driver(config, logger)
    const fileName = `${string.random(10)}.txt`

    try {
      await driver.getVisibility(fileName)
    } catch (error) {
      assert.equal(
        error.message,
        `Unable to retrieve the "visibility" for file at location "${fileName}"`
      )
      assert.equal(error.code, 'E_CANNOT_GET_METADATA')
    }
  }).timeout(6000)
})

test.group('S3 driver | setVisibility', (group) => {
  group.each.teardown(async () => {
    await fs.cleanup()
  })

  test('set file visibility', async ({ assert }) => {
    const fileName = `${string.random(10)}.txt`

    const driver = new S3Driver(config, logger)
    await driver.put(fileName, 'hello world')
    assert.equal(await driver.getVisibility(fileName), 'private')

    await driver.setVisibility(fileName, 'public')
    assert.equal(await driver.getVisibility(fileName), 'public')

    await driver.delete(fileName)
  }).timeout(6000)

  test('return error when file is missing', async ({ assert }) => {
    assert.plan(2)

    const driver = new S3Driver(config, logger)
    const fileName = `${string.random(10)}.txt`

    try {
      await driver.setVisibility(fileName, 'public')
    } catch (error) {
      assert.equal(error.message, `Unable to set visibility for file at location "${fileName}"`)
      assert.equal(error.code, 'E_CANNOT_SET_VISIBILITY')
    }
  }).timeout(6000)
})
