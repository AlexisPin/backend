import { DateTime } from 'luxon'
import { BaseModel, belongsTo, column } from '@adonisjs/lucid/orm'
import { type BelongsTo } from '@adonisjs/lucid/types/relations'
import DungeonType from './dungeon_type.js'
import type { UUID } from '#types/common'

export default class Monster extends BaseModel {
  @column({ isPrimary: true })
  declare id: UUID

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime

  @column()
  declare name: string

  @column()
  declare hp: number

  @column()
  declare attack: number

  @column()
  declare defense: number

  @column()
  declare dungeonTypeId: number

  @belongsTo(() => DungeonType)
  declare dungeonType: BelongsTo<typeof DungeonType>
}
