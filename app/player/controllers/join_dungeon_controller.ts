import { HttpContext } from '@adonisjs/core/http'
import JoinDungeonAction from '../actions/join_dungeon_action.js'
import { assert } from '../../utils.js'
import { inject } from '@adonisjs/core'

@inject()
export default class JoinDungeonController {
  constructor(private action: JoinDungeonAction) { }

  async handle({ auth, response, params }: HttpContext) {
    const user = auth.user
    assert(user, 'User must be logged in to join a dungeon')
    const dungeonId = params.id
    const userId = user.id
    const dungeon = await this.action.execute({ dungeonId, userId })
    return response.ok(dungeon)
  }
}
