import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'dungeon_monsters'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.db.rawQuery('gen_random_uuid()').knexQuery)
      table.timestamp('created_at', { useTz: false })
      table.timestamp('updated_at', { useTz: false })
      table
        .uuid('dungeon_stage_id')
        .unsigned()
        .references('id')
        .inTable('dungeon_stages')
        .onDelete('CASCADE')
      table
        .uuid('monster_id')
        .unsigned()
        .references('id')
        .inTable('monsters')
        .onDelete('CASCADE')
      table.integer('level').unsigned().checkPositive()
      table.integer('remaining_hp').unsigned().checkPositive()
      table.integer('spell_attack_effect').unsigned().checkPositive()
      table.integer('spell_defense_effect').unsigned().checkPositive()
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
