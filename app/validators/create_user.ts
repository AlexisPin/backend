import vine, { SimpleMessagesProvider } from '@vinejs/vine'
import { uniqueRule } from './unique.js'

export const createUserValidator = vine.compile(
  vine.object({
    email: vine
      .string()
      .trim()
      .toLowerCase()
      .maxLength(255)
      .email()
      .use(
        uniqueRule({
          table: 'users',
          column: 'email',
        })
      ),
    username: vine
      .string()
      .trim()
      .minLength(4)
      .maxLength(25)
      .use(
        uniqueRule({
          table: 'users',
          column: 'username',
        })
      ),
    password: vine.string().trim().minLength(8).confirmed(),
  })
)

createUserValidator.messagesProvider = new SimpleMessagesProvider({
  'username.required': 'You need to specify a username.',
  'username.unique': 'An account already exists with the provided username.',
  'username.minLength': 'Your username needs to be at least 4 characters long.',
  'username.maxLength': 'Your username cannot be longer than 25 characters.',
  'email.required': 'You need to specify an email.',
  'email.unique': 'An account already exists with the provided email.',
  'email.maxLength': 'Your email cannot be longer than 255 characters.',
  'email.email': 'Your email is not valid.',
  'password.required': 'You need to specify a password.',
  'password.minLength': 'Your password needs to be at least 8 characters long.',
  'password.confirmed': 'Passwords do not match.',
})
