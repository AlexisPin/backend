import { BaseModel, belongsTo, column, hasMany, hasOne } from '@adonisjs/lucid/orm'
import { type HasOne, type BelongsTo, type HasMany } from '@adonisjs/lucid/types/relations'
import DungeonType from '#models/dungeon_type'
import { DateTime } from 'luxon'
import DungeonRank from './dungeon_rank.js'
import Player from './player.js'
import DungeonStage from './dungeon_stage.js'
import type { UUID } from '#types/common'

export default class Dungeon extends BaseModel {
  @column({ isPrimary: true })
  declare id: UUID

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime

  @column()
  declare typeId: number

  @belongsTo(() => DungeonType)
  declare type: BelongsTo<typeof DungeonType>

  @column()
  declare rankId: number

  @belongsTo(() => DungeonRank)
  declare rank: BelongsTo<typeof DungeonRank>

  @column()
  declare stage_number: number

  @column()
  declare name: string

  @hasOne(() => Player)
  declare player: HasOne<typeof Player>

  @hasMany(() => DungeonStage)
  declare stages: HasMany<typeof DungeonStage>
}
