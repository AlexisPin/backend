import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'inventory_item'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.db.rawQuery('gen_random_uuid()').knexQuery)
      table.integer('quantity').checkPositive().notNullable()
      table.uuid('item_id').references('id').inTable('items').notNullable()
      table.uuid('inventory_id').references('id').inTable('inventories').notNullable()
      table.unique(['item_id', 'inventory_id'])
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
