const AuthController = () => import('#auth/controllers/auth_controller')
import router from '@adonisjs/core/services/router'
import { middleware } from './kernel.js'
const GetPlayerSpellsController = () => import('#player/controllers/get_player_spells_controller')
const GetPlayerInventoryController = () =>
  import('#player/controllers/get_player_inventory_controller')
const GetPlayerDungeonController = () => import('#player/controllers/get_player_dungeon_controller')
const JoinDungeonController = () => import('#player/controllers/join_dungeon_controller')
const GetPlayerController = () => import('#player/controllers/get_player_controller')
const GetPersistantPlayerController = () =>
  import('#player/controllers/get_persistent_player_controller')

router.get('/', ({ response }) => response.ok({ uptime: Math.round(process.uptime()) }))
router.get('health', ({ response }) => response.noContent())

router.get('auth/check', [AuthController, 'check'])
router.post('auth/register', [AuthController, 'register'])
router.post('auth/login', [AuthController, 'login'])

router
  .group(() => {
    router.get('auth/me', [AuthController, 'me'])
    router.delete('auth/logout', [AuthController, 'logout'])
  })
  .use(middleware.auth())

router
  .group(() => {
    router.get('persistent-player', [GetPersistantPlayerController, 'handle'])
    router.get('player', [GetPlayerController, 'handle'])
    router.get('player/dungeon', [GetPlayerDungeonController, 'handle'])
    router.get('player/inventory', [GetPlayerInventoryController, 'handle'])
    router.get('player/spells', [GetPlayerSpellsController, 'handle'])
    router
      .get('dungeon/:id/join', [JoinDungeonController, 'handle'])
      .where('id', router.matchers.uuid())
  })
  .use(middleware.auth())
