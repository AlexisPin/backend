import User from '#auth/models/user'
import Inventory from '#models/inventory'
import { inject } from '@adonisjs/core'
import GetPlayerAction from './get_player_action.js'

@inject()
export default class GetPlayerInventoryAction {
  constructor(private getPlayerAction: GetPlayerAction) { }

  async execute(user: User) {
    const player = await this.getPlayerAction.execute(user)

    const inventory = await Inventory.findBy('playerId', player.id)

    if (!inventory) {
      return await player.related('inventory').create({})
    }

    await inventory.load('items')

    return inventory
  }
}
