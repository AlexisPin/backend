import { S3Driver } from '#drivers/s3'
import Spell from '#models/spell'
import env from '#start/env'
import logger from '@adonisjs/core/services/logger'

export default class PlayerSpellsPresenter {
  #config = {
    key: env.get('AWS_KEY'),
    secret: env.get('AWS_SECRET'),
    token: env.get('AWS_TOKEN'),
    bucket: env.get('AWS_BUCKET'),
    endpoint: env.get('AWS_ENDPOINT'),
    region: env.get('AWS_REGION'),
    driver: 's3' as const,
    visibility: 'private' as const,
  }

  async json(spells: Spell[]) {
    const s3 = new S3Driver(this.#config, logger)

    const serializedSpells = Promise.all(
      spells.map(async (spell) => {
        await spell.load('type')
        return {
          id: spell.id,
          name: spell.name,
          description: spell.description,
          damage: spell.damage,
          healing: spell.healing,
          cover: await s3.getSignedUrl(spell.cover),
          type: spell.type,
        }
      })
    )

    return {
      data: await serializedSpells,
    }
  }
}
