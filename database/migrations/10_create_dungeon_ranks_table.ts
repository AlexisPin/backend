import { BaseSchema } from '@adonisjs/lucid/schema'
import DungeonRank from '#enums/dungeon_rank'

export default class extends BaseSchema {
  protected tableName = 'dungeon_ranks'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name').notNullable()
    })

    this.defer(async (db) => {
      await db.table(this.tableName).multiInsert([
        { id: DungeonRank.S, name: 'S' },
        { id: DungeonRank.A, name: 'A' },
        { id: DungeonRank.B, name: 'B' },
        { id: DungeonRank.C, name: 'C' },
        { id: DungeonRank.D, name: 'D' },
        { id: DungeonRank.E, name: 'E' },
      ])
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
