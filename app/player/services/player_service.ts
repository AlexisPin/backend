import Player from '#models/player'
import { UUID } from '#types/common'
import { inject } from '@adonisjs/core'

@inject()
export default class PlayerService {
  update(playerId: UUID, options: Partial<Player>) {
    Player.query().where('id', playerId).update(options)
  }
}
