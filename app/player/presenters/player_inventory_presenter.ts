import { S3Driver } from '#drivers/s3'
import Inventory from '#models/inventory'
import env from '#start/env'
import logger from '@adonisjs/core/services/logger'

export default class PlayerInventoryPresenter {
  #config = {
    key: env.get('AWS_KEY'),
    secret: env.get('AWS_SECRET'),
    token: env.get('AWS_TOKEN'),
    bucket: env.get('AWS_BUCKET'),
    endpoint: env.get('AWS_ENDPOINT'),
    region: env.get('AWS_REGION'),
    driver: 's3' as const,
    visibility: 'private' as const,
  }

  async json(inventory: Inventory) {
    const s3 = new S3Driver(this.#config, logger)

    const serializedItems = Promise.all(
      inventory.items.map(async (item) => ({
        id: item.id,
        name: item.name,
        description: item.description,
        price: item.price,
        cover: await s3.getSignedUrl(item.cover),
        quantity: item.$extras.pivot_quantity,
      }))
    )

    return {
      data: {
        items: await serializedItems,
      },
    }
  }
}
