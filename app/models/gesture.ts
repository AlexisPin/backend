import { DateTime } from 'luxon'
import { BaseModel, belongsTo, column } from '@adonisjs/lucid/orm'
import Spell from './spell.js'
import { type BelongsTo } from '@adonisjs/lucid/types/relations'
import type { UUID } from '#types/common'

export default class Gesture extends BaseModel {
  @column({ isPrimary: true })
  declare id: UUID

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime

  @column()
  declare name: string

  @column()
  declare cover: string

  @column()
  declare spellId: UUID

  @belongsTo(() => Spell)
  declare spell: BelongsTo<typeof Spell>
}
