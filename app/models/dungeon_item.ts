import { DateTime } from 'luxon'
import { BaseModel, belongsTo, column } from '@adonisjs/lucid/orm'
import DungeonStage from './dungeon_stage.js'
import { type BelongsTo } from '@adonisjs/lucid/types/relations'
import Item from './item.js'
import type { UUID } from '#types/common'

export default class DungeonItem extends BaseModel {
  @column({ isPrimary: true })
  declare id: UUID

  @column.dateTime({ autoCreate: true })
  declare createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  declare updatedAt: DateTime

  @column()
  declare dungeonStageId: UUID

  @belongsTo(() => DungeonStage)
  declare dungeonStage: BelongsTo<typeof DungeonStage>

  @column()
  declare itemId: UUID

  @belongsTo(() => Item)
  declare item: BelongsTo<typeof Item>
}
