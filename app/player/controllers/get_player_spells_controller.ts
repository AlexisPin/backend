import { inject } from '@adonisjs/core'
import PlayerSpellsPresenter from '#player/presenters/player_spells_presenter'
import GetPlayerSpellsAction from '../actions/get_player_spells_action.js'
import { HttpContext } from '@adonisjs/core/http'
import { assert } from '../../utils.js'

@inject()
export default class GetPlayerSpellsController {
  constructor(
    private action: GetPlayerSpellsAction,
    private presenter: PlayerSpellsPresenter
  ) { }
  async handle({ auth, response }: HttpContext) {
    const user = auth.user
    assert(user, 'User must be logged in to get spells player')

    const spells = await this.action.execute(user)

    return response.ok(await this.presenter.json(spells))
  }
}
