import User from '#auth/models/user'
import { inject } from '@adonisjs/core'
import GetPersistentPlayerAction from './get_persistent_player_action.js'
import Player from '#models/player'

@inject()
export default class GetPlayerAction {
  constructor(private getPeristentPlayerAction: GetPersistentPlayerAction) { }

  async execute(user: User) {
    const persistentPlayer = await this.getPeristentPlayerAction.execute(user)

    let player = await Player.findBy('persistentPlayerId', persistentPlayer.id)

    if (!player) {
      player = await persistentPlayer.related('player').create({
        constitution: 10,
        wisdom: 10,
        intelligence: 10,
        health: 100,
        gold: 0,
        level: 1,
        xp: 0,
        dungeonId: null,
      })
    }

    return player
  }
}
