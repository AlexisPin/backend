import { BaseSchema } from '@adonisjs/lucid/schema'
import DungeonType from '#enums/dungeon_type'

export default class extends BaseSchema {
  protected tableName = 'dungeon_types'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name').notNullable()
    })

    this.defer(async (db) => {
      await db.table(this.tableName).multiInsert([
        { id: DungeonType.DUNGEON, name: 'Dungeon' },
        { id: DungeonType.FOREST, name: 'Forest' },
        { id: DungeonType.CAVE, name: 'Cave' },
        { id: DungeonType.TOWER, name: 'Tower' },
        { id: DungeonType.CASTLE, name: 'Castle' },
        { id: DungeonType.ARENA, name: 'Arena' },
        { id: DungeonType.GUILD, name: 'Guild' },
        { id: DungeonType.RAID, name: 'Raid' },
      ])
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
