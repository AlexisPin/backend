import { BaseSchema } from '@adonisjs/lucid/schema'
import SpellType from '#enums/spell_type'

export default class extends BaseSchema {
  protected tableName = 'spell_types'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name').notNullable()
      table.string('description').notNullable()
      table.json('cover').nullable()
    })

    this.defer(async (db) => {
      await db.table(this.tableName).multiInsert([
        {
          id: SpellType.AIR,
          name: 'Air',
          description: 'Air spells are spells that manipulate air, wind, and weather.',
        },
        {
          id: SpellType.DARK,
          name: 'Dark',
          description: 'Dark spells are spells that manipulate darkness, shadows, and the void.',
        },
        {
          id: SpellType.EARTH,
          name: 'Earth',
          description: 'Earth spells are spells that manipulate earth, stone, and nature.',
        },
        {
          id: SpellType.FIRE,
          name: 'Fire',
          description: 'Fire spells are spells that manipulate fire, heat, and lava.',
        },
        {
          id: SpellType.LIGHT,
          name: 'Light',
          description: 'Light spells are spells that manipulate light, lightning, and electricity.',
        },
        {
          id: SpellType.SHADOW,
          name: 'Shadow',
          description: 'Shadow spells are spells that manipulate darkness, shadows, and the void.',
        },
        {
          id: SpellType.SPIRIT,
          name: 'Spirit',
          description:
            'Spirit spells are spells that manipulate spirits, souls, and the afterlife.',
        },
        {
          id: SpellType.WATER,
          name: 'Water',
          description: 'Water spells are spells that manipulate water, ice, and steam.',
        },
      ])
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
