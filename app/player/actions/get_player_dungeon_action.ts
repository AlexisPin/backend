import User from '#auth/models/user'
import { inject } from '@adonisjs/core'
import GetPlayerAction from './get_player_action.js'
import Dungeon from '#models/dungeon'

@inject()
export default class GetPlayerDungeonAction {
  constructor(private getPlayerAction: GetPlayerAction) { }

  async execute(user: User) {
    const player = await this.getPlayerAction.execute(user)

    const dungeon = await Dungeon.findOrFail(player.dungeonId)

    return dungeon
  }
}
