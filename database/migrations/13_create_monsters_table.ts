import { BaseSchema } from '@adonisjs/lucid/schema'

export default class extends BaseSchema {
  protected tableName = 'monsters'

  async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.uuid('id').primary().defaultTo(this.db.rawQuery('gen_random_uuid()').knexQuery)
      table.timestamp('created_at')
      table.timestamp('updated_at')
      table.string('name')
      table.integer('hp').unsigned().checkPositive()
      table.integer('attack').unsigned().checkPositive()
      table.integer('defense').unsigned().checkPositive()
      table
        .integer('dungeon_type_id')
        .unsigned()
        .references('id')
        .inTable('dungeon_types')
        .onDelete('CASCADE')
    })
  }

  async down() {
    this.schema.dropTable(this.tableName)
  }
}
