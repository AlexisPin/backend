import { ExceptionHandler, HttpContext } from '@adonisjs/core/http'
import { HttpError } from '@adonisjs/core/types/http'
import { DateTime } from 'luxon'

export class DungeonNotFoundException extends ExceptionHandler {
  constructor(private dungeonId: string) {
    super()
  }

  async handle(_error: unknown, ctx: HttpContext) {
    return ctx.response.notFound({
      status: 404,
      path: ctx.request.url(),
      timestamp: DateTime.local(),
      code: 'E_DUNGEON_NOT_FOUND',
      message: `The requested dungeon ${this.dungeonId} was not found`,
      detail: 'Ensure that the dungeon exists',
    })
  }
}
