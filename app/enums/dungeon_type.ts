enum DungeonType {
  DUNGEON = 1,
  FOREST = 2,
  CAVE = 3,
  TOWER = 4,
  CASTLE = 5,
  ARENA = 6,
  GUILD = 7,
  RAID = 8,
}

export default DungeonType
