import { DungeonNotFoundException } from '#exceptions/index'
import Dungeon from '#models/dungeon'
import type { UUID } from '#types/common'

interface JoinDungeonActionParams {
  dungeonId: UUID
  userId: UUID
}

export default class JoinDungeonAction {
  async execute(params: JoinDungeonActionParams) {
    const { dungeonId } = params
    const dungeon = await Dungeon.findBy('id', dungeonId)
    if (!dungeon) {
      throw new DungeonNotFoundException(dungeonId)
    }
    return dungeon
  }
}
