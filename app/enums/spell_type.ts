enum SpellType {
  FIRE = 1,
  WATER = 2,
  EARTH = 3,
  AIR = 4,
  LIGHT = 5,
  DARK = 6,
  SPIRIT = 7,
  SHADOW = 8,
}

export default SpellType
