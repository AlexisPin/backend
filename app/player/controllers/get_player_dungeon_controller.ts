import { inject } from '@adonisjs/core'
import { HttpContext } from '@adonisjs/core/http'
import GetPlayerDungeonAction from '#player/actions/get_player_dungeon_action'
import { assert } from '../../utils.js'

@inject()
export default class GetPlayerDungeonController {
  constructor(private action: GetPlayerDungeonAction) { }

  async handle({ auth, response }: HttpContext) {
    const user = auth.user
    assert(user, 'User must be logged in to get dungeon player')

    const dungeon = await this.action.execute(user)
    
    return response.ok(dungeon)
  }
}
