import SpellType from '#enums/spell_type'

export const spells = [
  {
    name: 'Boule de feu',
    description: 'Lance une boule de feu qui inflige 30 points de dégâts de feu.',
    damage: 30,
    healing: 0,
    spellTypeId: SpellType.FIRE,
  },
  {
    name: 'Soin',
    description: 'Soigne le lanceur ou une créature alliée de 15 points de vie.',
    damage: 0,
    healing: 15,
    spellTypeId: SpellType.LIGHT,
  },
  {
    name: 'Mur de pierre',
    description: 'Crée un mur de pierre de 10 pieds de hauteur et 20 pieds de longueur.',
    damage: 0,
    healing: 0,
    spellTypeId: SpellType.EARTH,
  },
  {
    name: 'Vent glacial',
    description: 'Génère un vent glacial qui inflige 20 points de dégâts de froid.',
    damage: 20,
    healing: 0,
    spellTypeId: SpellType.AIR,
  },
  {
    name: 'Lueur',
    description: 'Éclaire une zone de 30 pieds.',
    damage: 0,
    healing: 0,
    spellTypeId: SpellType.LIGHT,
  },
  {
    name: 'Ténèbres',
    description: 'Obscurcit une zone de 30 pieds.',
    damage: 0,
    healing: 0,
    spellTypeId: SpellType.DARK,
  },
  {
    name: 'Esprit familier',
    description: 'Invoque un esprit familier qui assiste le lanceur.',
    damage: 0,
    healing: 0,
    spellTypeId: SpellType.SPIRIT,
  },
  {
    name: 'Ombre',
    description: 'Crée une ombre qui attaque une créature ennemie.',
    damage: 20,
    healing: 0,
    spellTypeId: SpellType.SHADOW,
  },
  {
    name: 'Vampirisme',
    description:
      'Inflige 10 points de dégâts de ténèbres et soigne le lanceur de 10 points de vie.',
    damage: 10,
    healing: 10,
    spellTypeId: SpellType.DARK,
  },
]
