import { BaseModel, belongsTo, column, hasOne, manyToMany } from '@adonisjs/lucid/orm'
import SpellType from './spell_type.js'
import type { HasOne, BelongsTo, ManyToMany } from '@adonisjs/lucid/types/relations'
import Gesture from './gesture.js'
import Model from './model.js'
import type { UUID } from '#types/common'

export default class Spell extends BaseModel {
  @column({ isPrimary: true })
  declare id: UUID

  @column()
  declare name: string

  @column()
  declare description: string

  @column()
  declare cover: string

  @column()
  declare damage: number

  @column()
  declare healing: number

  @column()
  declare spellTypeId: number

  @belongsTo(() => SpellType)
  declare type: BelongsTo<typeof SpellType>

  @hasOne(() => Gesture)
  declare gesture: HasOne<typeof Gesture>

  @manyToMany(() => Model)
  declare models: ManyToMany<typeof Model>
}
