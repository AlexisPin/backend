import { HttpContext } from '@adonisjs/core/http'
import { assert } from '../../utils.js'
import GetPlayerAction from '#player/actions/get_player_action'
import { inject } from '@adonisjs/core'
import PlayerPresenter from '#player/presenters/player_presenter'

@inject()
export default class GetPlayerController {
  constructor(
    private action: GetPlayerAction,
    private presenter: PlayerPresenter
  ) { }

  async handle({ auth, response }: HttpContext) {
    const user = auth.user
    assert(user, 'User must be logged in to get dungeon player')

    const player = await this.action.execute(user)
    
    return response.ok(this.presenter.json(player))
  }
}
