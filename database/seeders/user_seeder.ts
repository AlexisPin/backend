import User from '#auth/models/user'
import { BaseSeeder } from '@adonisjs/lucid/seeders'

export default class extends BaseSeeder {
  async run() {
    await User.create({
      email: 'test@example.com',
      password: '12345678',
      username: 'test',
    })
  }
}
