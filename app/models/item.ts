import type { UUID } from '#types/common'
import { BaseModel, column } from '@adonisjs/lucid/orm'

export default class Item extends BaseModel {
  @column({ isPrimary: true })
  declare id: UUID

  @column()
  declare name: string

  @column()
  declare description: string

  @column()
  declare cover: string

  @column()
  declare price: number
}
