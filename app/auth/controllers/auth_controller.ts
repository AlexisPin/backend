import User from '#auth/models/user'
import { createUserValidator } from '#validators/create_user'
import { HttpContext } from '@adonisjs/core/http'

export default class AuthController {
  me({ auth }: HttpContext) {
    return auth.user
  }

  async check({ auth, response }: HttpContext) {
    return response.ok({ authenticated: await auth.use('web').check() })
  }

  async login({ auth, request, response }: HttpContext) {
    const { email, password } = request.all()

    await auth.use('web').attempt(email, password)

    return response.noContent()
  }

  async register({ request, auth, response }: HttpContext) {
    const payload = await createUserValidator.validate(request.all(), {
      messagesProvider: createUserValidator.messagesProvider,
    })

    const user = await User.create(payload)

    await auth.use('web').login(user)

    return response.created()
  }

  async logout({ auth, response }: HttpContext) {
    await auth.use('web').logout()

    return response.noContent()
  }
}
