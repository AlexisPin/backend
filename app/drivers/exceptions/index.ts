import { Exception } from '@poppinss/utils'

/**
 * Unable to write file to the destination
 */
export class CannotWriteFileException extends Exception {
  location?: string
  original: any

  static invoke(location: string, original: any) {
    console.log(original)
    const error = new this(`Cannot write file at location "${location}"`, {
      code: 'E_CANNOT_WRITE_FILE',
      status: 500,
    })

    error.location = location
    error.original = original

    return error
  }
}

/**
 * Unable to read file from a given location
 */
export class CannotReadFileException extends Exception {
  location?: string
  original: any

  static invoke(location: string, original: any) {
    const error = new this(`Cannot read file from location "${location}"`, {
      code: 'E_CANNOT_READ_FILE',
      status: 500,
    })

    error.location = location
    error.original = original

    return error
  }
}

/**
 * Unable to delete file from a given location
 */
export class CannotDeleteFileException extends Exception {
  location?: string
  original: any

  static invoke(location: string, original: any) {
    const error = new this(`Cannot delete file at location "${location}"`, {
      code: 'E_CANNOT_DELETE_FILE',
      status: 500,
    })

    error.location = location
    error.original = original

    return error
  }
}

/**
 * Unable to copy file from source to destination
 */
export class CannotCopyFileException extends Exception {
  source?: string
  destination?: string
  original: any

  static invoke(source: string, destination: string, original: any) {
    const error = new this(`Cannot copy file from "${source}" to "${destination}"`, {
      code: 'E_CANNOT_COPY_FILE',
      status: 500,
    })

    error.source = source
    error.destination = destination
    error.original = original

    return error
  }
}

/**
 * Unable to move file from source to destination
 */
export class CannotMoveFileException extends Exception {
  source?: string
  destination?: string
  original: any

  static invoke(source: string, destination: string, original: any) {
    const error = new this(`Cannot move file from "${source}" to "${destination}"`, {
      code: 'E_CANNOT_MOVE_FILE',
      status: 500,
    })

    error.source = source
    error.destination = destination
    error.original = original

    return error
  }
}

/**
 * Unable to get file metadata
 */
export class CannotGetMetaDataException extends Exception {
  location?: string
  operation?: string
  original: any

  static invoke(location: string, operation: string, original: any) {
    const error = new this(
      `Unable to retrieve the "${operation}" for file at location "${location}"`,
      {
        code: 'E_CANNOT_GET_METADATA',
        status: 500,
      }
    )

    error.location = location
    error.operation = operation
    error.original = original

    return error
  }
}

/**
 * Unable to set visibility for a file
 */
export class CannotSetVisibilityException extends Exception {
  location?: string
  original: any

  static invoke(location: string, original: any) {
    const error = new this(`Unable to set visibility for file at location "${location}"`, {
      code: 'E_CANNOT_SET_VISIBILITY',
      status: 500,
    })

    error.location = location
    error.original = original

    return error
  }
}
